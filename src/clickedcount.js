import React from "react";
import withCounterHOC from "./withCounterHOC";

class Clickedcount extends React.Component {
  render() {
    return (
      <button onClick={this.props.incc}>
        Clicked {this.props.count} Times
      </button>
    );
  }
}

export default withCounterHOC(Clickedcount);
