import React, { useState, useMemo } from "react";

function Usememo() {
  const [number, setNumber] = useState(0);
  const [counter, setCounter] = useState(0);

  const squaredNum = useMemo(() => {
    return squareNum(number);
  }, [number]);

  const counterHander = () => {
    setCounter(counter + 1);
    console.log("done");
  };

  return (
    <>
      <input
        type="number"
        placeholder="Enter a number"
        value={number}
        onChange={(e) => setNumber(e.target.value)}
      ></input>

      <div>OUTPUT: {squaredNum}</div>
      <button onClick={counterHander}>Add</button>
      <div>Counter : {counter}</div>
    </>
  );
}

// function to square the value
function squareNum(number) {
  console.log("Squaring will be done!");
  return Math.pow(number, 2);
}

export default Usememo;
