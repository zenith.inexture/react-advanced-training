import React from "react";

//this ErrorBoundary component work as a catch box in javascript
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false };
  }

  //when error comes
  componentDidCatch() {
    // Catch errors in any components below and re-render with error message
    // we can write this setState in getDerivedFromPorps also
    this.setState({
      error: true,
    });
    // You can also log error messages to an error reporting service here
  }

  render() {
    if (this.state.error) {
      // Error path
      return <h2>Something went wrong.</h2>;
    }
    // Normally, just render children so otherwise return Counter component
    return this.props.children;
  }
}

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = { counter: 0 };
  }

  handleClick() {
    this.setState((precounter) => ({
      counter: precounter.counter + 1,
    }));
  }

  render() {
    if (this.state.counter === 5) {
      // Simulate a JS error
      throw new Error(); //throw an error
    }
    return (
      <button
        onClick={() => {
          this.handleClick();
        }}
      >
        {this.state.counter}
      </button>
    );
  }
}

//error boundary is only for class component
// Error boundaries do not catch errors inside event handlers.
function Error() {
  return (
    <ErrorBoundary>
      <Counter />
      {/* Counter component has error boundary */}
    </ErrorBoundary>
  );
}

export default Error;
