import React from "react";
import { Usercontext } from "./userContext";

function D() {
  const user = React.useContext(Usercontext);
  return (
    <div>
      <h1>{user}</h1>
    </div>
  );
}

export default D;
