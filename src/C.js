import React from "react";
import { UserConsumer } from "./userContext";

class C extends React.Component {
  render() {
    return (
      <UserConsumer>
        {/* consumer always fetch the value in variable and then return  */}
        {/* and consumer always have return statement  */}
        {(hello) => <h1>hello {hello}</h1>}
      </UserConsumer>
    );
  }
}
export default C;
