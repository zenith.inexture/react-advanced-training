import React from "react";

function FetchData() {
  const [data, setData] = React.useState([]);
  const [isloading, setLoading] = React.useState(true);
  React.useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((data) => setData(data), setLoading(false))
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      {isloading ? (
        <div class="spinner-border" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      ) : (
        data.map((i) => <span key={i.id}>{i.name} </span>)
      )}
    </>
  );
}

export default FetchData;
