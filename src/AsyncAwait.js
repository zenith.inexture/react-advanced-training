import React from "react";

// function AsyncAwait() {
//   const [name, setName] = React.useState("Zenith");
//   async function Helloworld() {
//     let myPromise = new Promise(function (resolve, reject) {
//       resolve("Poriya");
//     });
//     setName(await myPromise);
//   }
//   return (
//     <>
//       AsyncAwait function : <button onClick={Helloworld}>{name}</button>
//     </>
//   );
// }

class AsyncAwait extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "Zenith" };
    this.Helloworld = this.Helloworld.bind(this);
  }
  async Helloworld() {
    let myPromise = new Promise(function (resolve, reject) {
      resolve("Poriya");
    });
    const result = await myPromise;
    this.setState({ name: result });
  }
  render() {
    return (
      <>
        AsyncAwait function :
        <button onClick={this.Helloworld}>{this.state.name}</button>
      </>
    );
  }
}

// class Function extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { name: "Zenith" };
//     this.asdfs = this.asdfs.bind(this);
//   }
//   run() {
//     //we can run this fuction without binding also
//     //but if we want to change the state in this function then it compalsary to bind this function
//     console.log("zenith poriya");
//     this.setState({ name: "Poriya" });
//   }
//   render() {
//     return (
//       <button type="submit" onClick={this.run}>
//         {this.state.name}
//       </button>
//     );
//   }
// }

export default AsyncAwait;
