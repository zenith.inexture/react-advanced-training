import React from "react";
import FRefComponent from "./FRefComponent";

class FRefPComponent extends React.Component {
  constructor(props) {
    super(props);
    this.inputref = React.createRef();
  }
  clickfocus() {
    this.inputref.current.focus();
  }
  render() {
    return (
      <>
        <FRefComponent ref={this.inputref} />
        <button onClick={() => this.clickfocus()}>Click me!!</button>
      </>
    );
  }
}

export default FRefPComponent;
