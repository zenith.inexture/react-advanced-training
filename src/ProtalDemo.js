import React from "react";
import { createPortal } from "react-dom";

//we can render the components into the different element in html page
function ProtalDemo() {
  return createPortal(
    <div>ProtalDemo</div>,
    document.getElementById("portaldemo")
  );
}

export default ProtalDemo;
