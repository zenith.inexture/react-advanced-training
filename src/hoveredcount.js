import React from "react";
import withCounterHOC from "./withCounterHOC";

class Hoveredcount extends React.Component {
  render() {
    return (
      <span onMouseOver={this.props.incc}>
        Hovered {this.props.count} Times
      </span>
    );
  }
}

export default withCounterHOC(Hoveredcount);
