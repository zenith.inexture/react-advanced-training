import React from "react";

class Fragments extends React.Component {
  render() {
    return (
      //   A common pattern in React is for a component to return multiple elements.
      //   Short Syntax - <></>
      <React.Fragment>
        <h3>Hello i'm tag one.</h3>
        <h3>Hello i'm tag two.</h3>
        <h3>Hello i'm tag three.</h3>
        <h3>Hello i'm tag four.</h3>
      </React.Fragment>
    );
  }
}

export default Fragments;
