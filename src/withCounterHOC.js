import React from "react";

//WithCounterHoc is new component and wrappedComponent is originla component
const withCounterHOC = (WrappedComponent) => {
  class WithCounterHOC extends React.Component {
    constructor(props) {
      super(props);
      this.state = { count: 0 };
    }
    inc = () => {
      this.setState((presate) => {
        return { count: presate.count + 1 };
      });
    };
    render() {
      return <WrappedComponent count={this.state.count} incc={this.inc} />;
    }
  }
  return WithCounterHOC;
};

export default withCounterHOC;
