import React from "react";
import B from "./B";

class A extends React.Component {
  render() {
    return <B />;
  }
}
export default A;
