import ReactDOM from "react-dom";
import Error from "./Error Boundaries";
import Fragments from "./Fragments";
import Ref from "./RefComponent";
import FRef from "./FRefPComponent";
import Hoveredcount from "./hoveredcount";
import Clickedcount from "./clickedcount";
import A from "./A";
import { UserProvider } from "./userContext";
import Usememo from "./Usememo";
import Usecallback from "./Usecallback";
import AsyncAwait from "./AsyncAwait";
import D from "./D";
import Usereducer from "./Usereducer";
import App from "./App";
import FetchData from "./FetchData";
//react lazy function let you render a dynamic import as regular component
//const OtherComponent = React.lazy(() => import('./OtherComponent'));

//Error Boundaries rander
ReactDOM.render(<Error />, document.getElementById("root1"));

//Fragments
ReactDOM.render(<Fragments />, document.getElementById("root2"));

//ref
ReactDOM.render(<Ref />, document.getElementById("root3"));

//forwarding ref
ReactDOM.render(<FRef />, document.getElementById("root4"));

//clicked and hovered counter with HOC
ReactDOM.render(<Clickedcount />, document.getElementById("root5"));
ReactDOM.render(<Hoveredcount />, document.getElementById("root6"));

//contextAPI
ReactDOM.render(
  <UserProvider value="zenitf">
    <A />
    <D />
  </UserProvider>,
  document.getElementById("root7")
);

//Usememo
ReactDOM.render(<Usememo />, document.getElementById("root8"));

//Usecallback
ReactDOM.render(<Usecallback />, document.getElementById("root9"));

ReactDOM.render(<AsyncAwait />, document.getElementById("root10"));

ReactDOM.render(<Usereducer />, document.getElementById("root11"));

ReactDOM.render(<App />, document.getElementById("root12"));

ReactDOM.render(<FetchData />, document.getElementById("root13"));
