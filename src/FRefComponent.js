import React from "react";

const FRefComponent = React.forwardRef((props, ref) => {
  return <input type="text" ref={ref}></input>;
});

export default FRefComponent;
