import React from "react";

// class RefComponent extends React.Component {
//   constructor(props) {
//     super(props);
//     this.inputrefe = React.createRef();
//   }

//   clickbtn() {
//     this.inputrefe.current.style.border = "1px dashed black";
//   }
//   render() {
//     return (
//       <>
//         {/*we can pass ref with class component also*/}
//         <input type="text" ref={this.inputrefe}></input>
//         <button onClick={() => this.clickbtn()}>Click me!!</button>
//       </>
//     );
//   }
// }

//ref in function component with useref hook
function RefComponent() {
  const inputref = React.useRef();
  const click = () => {
    inputref.current.focus();
  };

  return (
    <>
      <input type="text" ref={inputref}></input>
      {console.log("asdfasdf")}
      <button type="submit" onClick={click}>
        Focus
      </button>
    </>
  );
}

export default RefComponent;
