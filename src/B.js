import React from "react";
import C from "./C";
import { Usercontext } from "./userContext";

class B extends React.Component {
  render() {
    return (
      <>
        <p>hello {this.context}, in B component with the context type</p>
        <C />
      </>
    );
  }
}
//with the help of context type we can access the context data but limitation is only one data can accessed
B.contextType = Usercontext;
export default B;
